<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::create(2015, 5, 28, 0, 0, 0);


        for ($i=0; $i < 10; $i++) {
            $countryID = DB::table('countries')->inRandomOrder()->first();
            DB::table('members')->insert([

                'firstname' => str_random(8),
                'lastname' => str_random(8),
                'birthdate' => $date->subWeek(rand(1, 150))->format('Y-m-d H:i:s'),
                'report_subject' => str_random(299),
                'country_id' => $countryID->id,
                'phone' => '+'.rand(0,9).' ('.rand(0,9).rand(0,9).rand(0,9).') '.rand(0,9).rand(0,9).rand(0,9).'-'.rand(0,9).rand(0,9).rand(0,9).rand(0,9),
                'email' => str_random(10).'@gmail.com',
                'company' => str_random(20),
                'position' => str_random(20),
                'about_me' => str_random(100),
                'photo' => 'images/user-default.png',
                'show' => rand(0,1)
            ]);
        }
    }
}
