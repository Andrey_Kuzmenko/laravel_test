<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    public function member()
    {
        return $this->hasOne('App\Member');
    }
}

