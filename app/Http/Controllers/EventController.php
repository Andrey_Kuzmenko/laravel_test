<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Country;
use Illuminate\Support\Facades\Auth;


class EventController extends Controller
{
    public function index(Request $request) {
        $member = array();
        $idRow = $request->session()->get('id') ?? "";
        if ($idRow) {
            $member = Member::find($idRow);
        }
        $countries = Country::all();
        return view('index',["countries" => $countries,"idRow" => $idRow,'request' => $request, 'member' => $member]);
    }

    public function add(Request $request) {
        $this->validate($request, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'birthdate' => 'required|before:'.date('Y-d-m').'|date_format:Y-d-m',
            'report_subject' => 'required|string|max:300',
            'country_id' => 'required|exists:countries,id',
            'phone' =>  ['required', 'regex:/\+[0-9] \(\d{3}\) \d{3}-\d{4}/'],
            'email' => 'required|string|email|max:255|unique:members'
        ], [
            'email.unique' => 'This email already exists'
        ]);
        if ($request->id) {
            $member = Member::find($request->id);
        }   else {
            $member = new Member;
        }
            $member->firstname = $request->firstname;
            $member->lastname = $request->lastname;
            $member->birthdate = $request->birthdate;
            $member->report_subject = $request->report_subject;
            $member->country_id = $request->country_id;
            $member->phone = $request->phone;
            $member->email = $request->email;
            $member->save();
            session(['id' => $member->id]);
            return $member->id;
    }
    public function additional(Request $request) {

        $this->validate($request, [
            'idnext' => 'exists:members,id',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2000'
        ], [
            'photo.image' => 'This file type is incorrect, please select an image',
            'photo.mimes' => 'This file type is incorrect, please select an image',
            'photo.max' => 'Your image is too large, max file size is 2 MB'
        ]);

        $name ='';
        foreach ($request->file() as $file) {
            $name = time().'_'.$file->getClientOriginalName();
            $file->move('images', $name);
        }
        $member = Member::find($request->idnext);
        $member->company = $request->company;
        $member->position = $request->position;
        $member->about_me = $request->about_me;
        if ($name) {
            $member->photo = 'images/' . $name;
        }
        $member->save();
        $request->session()->flush();
    }

    public function list() {
        if (Auth::check()) {
            $data = Member::paginate(10);
        } else {
            $data = Member::where('show',1)->paginate(10);
        }
        return view('members',["data" => $data]);
    }

    public function hide(Request $request) {
        $id = $request->json()->all();
        $member_info = Member::find($id['id']);
        if ($member_info->show) {
            $member_info->show = 0;
        } else {
            $member_info->show = 1;
        }
        $member_info->save();
        $ids_request[]=array('id' =>$member_info->id, 'show' => $member_info->show);
        return $ids_request;

    }
}
