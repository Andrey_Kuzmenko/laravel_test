@include('header')
<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
        &nbsp;
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @guest
            <li><a href="{{ route('login') }}">Login</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        @endguest
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>
                <?php if (Auth::check()) { ?>
                <th>Show/Hide</th>
                <?php }?>
                <th>Photo</th>
                <th>Name</th>
                <th>Report subject</th>
                <th>Email</th>
                <?php if (Auth::check()) { ?>
                <th>Status</th>
                <?php }?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $item) { ?>
            <tr>
                <?php if (Auth::check()) { ?>
                    <th scope="row"><input type="checkbox" name="member" value="<?= $item->id?>"/></th>
                <?php }?>
                <td><?=$item->photo ?  "<img src='/".$item->photo."'  height='80' width='80'>" : '<img src="images/user-default.png" alt="default image" height=\'80\' width=\'80\'>' ?></td>
                <td><?=$item->firstname.' '.$item->lastname?></td>
                <td><?=$item->report_subject?></td>
                <td><a href="mailto:<?=$item->email?>"><?=$item->email?></a></td>
                <?php if (Auth::check()) { ?>
                    <td id="<?= $item->id?>"><?=$item->show ? 'Shown' :'Hidden' ?></td>
                <?php }?>
            </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>
</div>
<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-4">
    <?=$data->links()?>
    </div>
</div>
<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-4">

        <a class="btn btn-primary" href="/" role="button">Back to form</a>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("contentType", "application/x-www-form-urlencoded; charset=UTF-8");
        }
    });
    $("input[name='member']").click(function () {
        id = this.value;
        $.ajax({
            type: 'POST',
            url: '/show-hide',
            data: '{"id":'+JSON.stringify(id)+'}',
            processData: false,
            contentType: false,
            success: function (ids) {
                ids.forEach(function( id ) {
                    if (id['show'] ) {
                        $('#'+id['id']).html('Shown');
                    } else {
                        $('#'+id['id']).html('Hidden');
                    }
                });
                setTimeout(function (){$("input[name='member']:checked").prop('checked', false)}, 500);

            }
        });
    });
   /* $('#show-hide').click(function () {
       $("input[name='member']:checked").each(function() {ids.push($(this).val());});
       if (ids.length) {
           $.ajax({
               type: 'POST',
               url: '/show-hide',
               data: '{"ids":'+JSON.stringify(ids)+'}',
               processData: false,
               contentType: false,
               success: function (ids) {
                   ids.forEach(function( id ) {
                       if (id['show'] ) {
                           $('#'+id['id']).html('Show');
                       } else {
                           $('#'+id['id']).html('Hide');
                       }
                   });
                   $("input[name='member']:checked").prop('checked', false);
               }
           });
       }
    });*/
</script>